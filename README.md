# MovieDB App

MovieDB is a movie catalog app with created using the [MovieDB API].
This app uses a number of dependency to work properly:

  - MVVM design pattern
  - Hilt for dependency injection
  - Room
  - Retrofit
  - Glide
  - Coroutine
  - Android youtube player by [PierfrancescoSoffritti]

# Screenshot!
![screenshot](./screenshot/detail.png)



  [MovieDB API]: <https://developers.themoviedb.org/3>
  [PierfrancescoSoffritti]: <https://github.com/PierfrancescoSoffritti/android-youtube-player>
