package com.riftar.moviedb2021.utils

import android.content.Context
import androidx.room.Room
import com.riftar.moviedb2021.data.db.MovieDB
import com.riftar.moviedb2021.data.network.API
import com.riftar.moviedb2021.data.network.ApiHelper
import com.riftar.moviedb2021.data.network.ApiHelperImp
import com.riftar.moviedb2021.data.network.RetrofitFactory
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.android.components.ApplicationComponent
import dagger.hilt.android.internal.managers.ApplicationComponentManager
import dagger.hilt.android.qualifiers.ApplicationContext
import retrofit2.Retrofit
import javax.inject.Singleton

@Module
@InstallIn(ApplicationComponent::class)
class AppModule {
    @Provides
    @Singleton
    fun provideOkHttpClient() = RetrofitFactory.getClient()

    @Provides
    @Singleton
    fun provideApiService(retrofit: Retrofit) = retrofit.create(API::class.java)

    @Provides
    @Singleton
    fun provideApiHelper(apiHelper: ApiHelperImp): ApiHelper = apiHelper

    @Singleton
    @Provides
    fun provideAppDatabase(@ApplicationContext appContext: Context): MovieDB {
        return Room
            .databaseBuilder(appContext, MovieDB::class.java, "movie_db")
            .build()
    }
}