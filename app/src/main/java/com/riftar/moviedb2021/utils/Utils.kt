package com.riftar.moviedb2021.utils

import androidx.room.PrimaryKey
import com.google.gson.Gson
import com.google.gson.annotations.SerializedName
import com.google.gson.reflect.TypeToken
import com.riftar.moviedb2021.data.model.*

object Utils{
    fun List<Review>.toJson(): String? {
       try {
           val gson = Gson()
           return gson.toJson(this)
       } catch (e: Exception){
           return null
       }
    }

    fun String.toReview():List<Review>? {
        try {
            val listType = object : TypeToken<List<Review>>() {}.type
            return Gson().fromJson<List<Review>>(this, listType)
        } catch (e: Exception){
            return null
        }
    }

    fun String.toCast():List<Cast>? {
        try {
            val listType = object : TypeToken<List<Cast>>() {}.type
            return Gson().fromJson<List<Cast>>(this, listType)
        } catch (e: Exception){
            return null
        }
    }

    fun String.toTrailer():List<Trailer>? {
        try {
            val listType = object : TypeToken<List<Trailer>>() {}.type
            return Gson().fromJson<List<Trailer>>(this, listType)
        } catch (e: Exception){
            return null
        }
    }

}