package com.riftar.moviedb2021.data.db.dao

import androidx.room.*
import com.riftar.moviedb2021.data.model.Movie
import com.riftar.moviedb2021.data.model.Review
import com.riftar.moviedb2021.data.model.response.GetReviewResponse
import kotlinx.coroutines.flow.Flow

@Dao
interface ReviewDao {
    @Query("SELECT results FROM tb_review WHERE id LIKE :movieid LIMIT 1")
    fun get(movieid: Int): Flow<String?>

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insert(review: GetReviewResponse)

    @Delete
    fun delete(review: GetReviewResponse)
}