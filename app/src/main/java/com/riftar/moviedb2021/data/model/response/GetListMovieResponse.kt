package com.riftar.moviedb2021.data.model.response

import com.google.gson.annotations.SerializedName
import com.riftar.moviedb2021.data.model.Movie

data class GetListMovieResponse (
    @SerializedName("page")
    val page : Int? = null,
    @SerializedName("results")
    val results: List<Movie>? = null
)