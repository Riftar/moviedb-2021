package com.riftar.moviedb2021.data.db.dao

import androidx.room.*
import com.riftar.moviedb2021.data.model.Movie
import kotlinx.coroutines.flow.Flow
import retrofit2.Response

@Dao
interface MovieDao {
    @Query("SELECT * FROM tb_movie")
    fun getAll(): Flow<List<Movie>>

    @Query("SELECT * FROM tb_movie")
    fun getAllFav(): Flow<List<Movie>>

    @Query("SELECT * FROM tb_movie WHERE id LIKE :movieid LIMIT 1")
    fun get(movieid: Int): Flow<Movie>

    @Query("SELECT * FROM tb_movie WHERE isLiked = 1")
    fun getFav(): Flow<List<Movie>>

    @Query("SELECT * FROM tb_movie WHERE category = :category ")
    fun getPopular(category: String): Flow<List<Movie>>

    @Query("SELECT * FROM tb_movie WHERE category = :category")
    fun getNowPlaying(category: String): Flow<List<Movie>>

    @Query("SELECT * FROM tb_movie WHERE category = :category")
    fun getTopRated(category: String): Flow<List<Movie>>

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insert(movies: Movie)

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insertAll(movies: List<Movie>)

    @Query("UPDATE tb_movie SET isLiked = :isLiked WHERE id = :movieId")
    fun updateLike(isLiked: Boolean, movieId: Int?)

    @Delete
    fun delete(movie: Movie)

    @Query("DELETE FROM tb_movie")
    fun deleteAll()
}