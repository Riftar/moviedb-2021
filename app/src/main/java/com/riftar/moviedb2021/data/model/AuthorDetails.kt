package com.riftar.moviedb2021.data.model

import androidx.room.Entity
import androidx.room.PrimaryKey
import com.google.gson.annotations.SerializedName
import java.io.Serializable

@Entity
data class AuthorDetails(
    @PrimaryKey(autoGenerate = true)
    var Id: Int? = null,
    @SerializedName("avatar_path")
    var avatarPath: String? = null
): Serializable
//{
//    constructor() : this(0, "")
//}