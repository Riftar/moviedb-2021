package com.riftar.moviedb2021.data.model

import androidx.room.Embedded
import androidx.room.Entity
import androidx.room.PrimaryKey
@Entity(tableName = "tb_fav")
data class Fav(
    @Embedded
    var movie: Movie? = null,

    @PrimaryKey(autoGenerate = false)
    var favId: Int? = null
)