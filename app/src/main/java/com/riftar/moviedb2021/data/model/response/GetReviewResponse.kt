package com.riftar.moviedb2021.data.model.response

import androidx.room.Entity
import androidx.room.PrimaryKey
import androidx.room.TypeConverters
import com.google.gson.annotations.SerializedName
import com.riftar.moviedb2021.data.db.converter.ReviewListConverter
import com.riftar.moviedb2021.data.model.Movie
import com.riftar.moviedb2021.data.model.Review
import java.io.Serializable

@Entity(tableName = "tb_review")
data class GetReviewResponse(
    @PrimaryKey(autoGenerate = false)
    @SerializedName("id")
    val id : Int? = null,
    @SerializedName("page")
    val page : Int? = null,
    @TypeConverters(ReviewListConverter::class)
    @SerializedName("results")
    val results: List<Review>? = null
) : Serializable