package com.riftar.moviedb2021.data.model
import androidx.room.Embedded
import androidx.room.Entity
import androidx.room.PrimaryKey
import androidx.room.TypeConverters
import com.google.gson.annotations.SerializedName
import com.riftar.moviedb2021.data.db.converter.AuthorConverter
import java.io.Serializable
data class Review(
    @SerializedName("author")
    var author: String? = null,

    @SerializedName("content")
    var content: String? = null,

    @TypeConverters(AuthorConverter::class)
    @SerializedName("author_details")
    var authorDetails: AuthorDetails? = null
 ): Serializable
