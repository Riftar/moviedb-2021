package com.riftar.moviedb2021.data.db

import androidx.room.Database
import androidx.room.RoomDatabase
import androidx.room.TypeConverters
import com.riftar.moviedb2021.data.db.converter.*
import com.riftar.moviedb2021.data.db.dao.*
import com.riftar.moviedb2021.data.model.*
import com.riftar.moviedb2021.data.model.response.GetCastsResponse
import com.riftar.moviedb2021.data.model.response.GetReviewResponse
import com.riftar.moviedb2021.data.model.response.GetTrailerResponse

@Database(entities = arrayOf(Movie::class, GetCastsResponse::class, GetTrailerResponse::class,
    GetReviewResponse::class, Fav::class), version = 1)
@TypeConverters(
    value = [
        (GenreListConverter::class), (CastListConverter::class), (ReviewListConverter::class),
        (AuthorConverter::class),(TrailerListConverter::class)
    ]
)
abstract class MovieDB : RoomDatabase() {
    abstract fun movieDao(): MovieDao
    abstract fun reviewDao(): ReviewDao
    abstract fun castDao(): CastDao
    abstract fun trailerDao(): TrailerDao
    abstract fun favDao(): FavDao
}