package com.riftar.moviedb2021.data.network

import com.riftar.moviedb2021.data.model.Movie
import com.riftar.moviedb2021.data.model.response.GetCastsResponse
import com.riftar.moviedb2021.data.model.response.GetReviewResponse
import com.riftar.moviedb2021.data.model.response.GetTrailerResponse
import com.riftar.moviedb2021.data.model.response.GetListMovieResponse
import retrofit2.Response

interface ApiHelper {
    suspend fun getPopular(page: Int): Response<GetListMovieResponse>
    suspend fun getNowPlaying(page: Int): Response<GetListMovieResponse>
    suspend fun getTopRated(page: Int): Response<GetListMovieResponse>
    suspend fun getMovie(movieID: Int): Response<Movie>
    suspend fun getTrailer(movieID: Int): Response<GetTrailerResponse>
    suspend fun getReview(movieID: Int): Response<GetReviewResponse>
    suspend fun getCast(movieID: Int): Response<GetCastsResponse>
}