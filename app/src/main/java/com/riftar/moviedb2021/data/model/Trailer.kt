package com.riftar.moviedb2021.data.model

import androidx.room.Entity
import androidx.room.PrimaryKey
import com.google.gson.annotations.SerializedName
import java.io.Serializable


data class Trailer(
    @SerializedName("key")
    var key: String? = null
): Serializable