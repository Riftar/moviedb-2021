package com.riftar.moviedb2021.data.repository

import android.util.Log
import com.riftar.moviedb2021.data.db.MovieDB
import com.riftar.moviedb2021.data.model.Fav
import com.riftar.moviedb2021.data.model.Movie
import com.riftar.moviedb2021.data.network.ApiHelper
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.coroutineScope
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.collect
import kotlinx.coroutines.flow.toList
import kotlinx.coroutines.launch
import retrofit2.Response
import javax.inject.Inject

class MovieRepository @Inject constructor(private val apiHelper: ApiHelper, private val movieDb: MovieDB){
    private val movieDao = movieDb.movieDao()
    private val favDao = movieDb.favDao()
    private lateinit var movies: Flow<List<Movie>>
    private lateinit var favorite: Flow<List<Movie>>
    private lateinit var favorite2: Flow<List<Fav>>
    private lateinit var movie: Flow<Movie>

    suspend fun getPopular(page: Int): Flow<List<Movie>> {
        refreshPopular(page)
        coroutineScope { launch(Dispatchers.IO) { movies = movieDao.getPopular("popular")}
        }
        return movies
    }

    private suspend fun refreshPopular(page: Int) {
        try {
            val response = apiHelper.getPopular(page)
            if (response.isSuccessful) {
                val listMovie = response.body()?.results
                if (listMovie != null) {
                    listMovie.forEach{
                        it.category = "popular"
                    }
                    coroutineScope {
                        launch(Dispatchers.IO) { movieDao.insertAll(listMovie)}
                    }
                }
            } else{
                Log.e("test", "refreshMovie: Error fetching data")
            }
        } catch (e: Exception){

        }
    }

    suspend fun getNowPlaying(page: Int): Flow<List<Movie>> {
        refreshNowPlaying(page)
        coroutineScope { launch(Dispatchers.IO) { movies = movieDao.getNowPlaying("now_playing")}
        }
        return movies
    }

    private suspend fun refreshNowPlaying(page: Int) {
        try {
            val response = apiHelper.getNowPlaying(page)
            if (response.isSuccessful) {
                val listMovie = response.body()?.results
                if (listMovie != null) {
                    listMovie.forEach{
                        it.category = "now_playing"
                    }
                    coroutineScope {
                        launch(Dispatchers.IO) { movieDao.insertAll(listMovie)}
                    }
                }
            } else{
                Log.e("test", "refreshMovie: Error fetching data")
            }
        } catch (e: Exception){

        }
    }

    suspend fun getTopRated(page: Int): Flow<List<Movie>> {
        refreshTopRated(page)
        coroutineScope { launch(Dispatchers.IO) { movies = movieDao.getTopRated("top_rated")}
        }
        return movies
    }

    private suspend fun refreshTopRated(page: Int) {
        try {
            val response = apiHelper.getTopRated(page)
            if (response.isSuccessful) {
                val listMovie = response.body()?.results
                if (listMovie != null) {
                    listMovie.forEach{
                        it.category = "top_rated"
                    }
                    coroutineScope {
                        launch(Dispatchers.IO) { movieDao.insertAll(listMovie)}
                    }
                }
            } else{
                Log.e("test", "refreshMovie: Error fetching data")
            }
        } catch (e: Exception){

        }
    }

    suspend fun getMovie(movieID: Int): Response<Movie> =  apiHelper.getMovie(movieID)

    suspend fun getMovieLocal(movieID: Int): Flow<Movie> {
        coroutineScope {
            launch(Dispatchers.IO){
               movie =  movieDao.get(movieID)
            }
        }
        return movie
    }

    suspend fun updateLike(isLike: Boolean, movieid:Int){
        coroutineScope {
            launch(Dispatchers.IO) {
                movieDao.updateLike(isLike, movieid) }
        }
    }


    suspend fun addToFav(movie:Movie){
        coroutineScope {
            launch(Dispatchers.IO) {
                val fav = Fav(movie, movie.id)
                favDao.insert(fav)
            }
        }
    }

    suspend fun getFavorite(): Flow<List<Fav>> {
        coroutineScope { launch(Dispatchers.IO) { favorite2 = favDao.getAllFav()}
        }
        return favorite2
    }

    suspend fun deleteFromFav(movie: Movie) {
        coroutineScope {
            launch(Dispatchers.IO) {
                val fav = Fav(movie, movie.id)
                favDao.delete(fav)
            }
        }
    }
}