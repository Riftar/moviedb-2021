package com.riftar.moviedb2021.data.repository

import android.util.Log
import com.riftar.moviedb2021.data.db.MovieDB
import com.riftar.moviedb2021.data.model.Movie
import com.riftar.moviedb2021.data.network.ApiHelper
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.coroutineScope
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.launch
import javax.inject.Inject

class DetailRepository @Inject constructor(private val apiHelper: ApiHelper, private val movieDb: MovieDB){
    private val castDao = movieDb.castDao()
    private val trailerDao = movieDb.trailerDao()
    private val reviewDao = movieDb.reviewDao()
    private lateinit var reviews: Flow<String?>
    private lateinit var cast: Flow<String?>
    private lateinit var trailer: Flow<String?>

    suspend fun getCast(movieID: Int): Flow<String?> {
        refreshCast(movieID)
        coroutineScope {
            launch(Dispatchers.IO)
            {
                cast = castDao.get(movieID)
            }
        }
        return cast
    }

    private suspend fun refreshCast(movieID: Int) {
        try {
            val response = apiHelper.getCast(movieID)
            if (response.isSuccessful) {
                val cast = response.body()
                if (cast != null) {
                    coroutineScope {
                        launch(Dispatchers.IO) {
                            castDao.insert(cast)
                        }
                    }
                }
            } else{
                Log.e("test", "refreshMovieDetail: Error fetching data")
            }
        } catch (e: Exception){

        }
    }

    suspend fun getReview(movieID: Int): Flow<String?> {
        refreshReview(movieID)
        coroutineScope {
            launch(Dispatchers.IO)
            {
                reviews = reviewDao.get(movieID)
            }
        }
        return reviews
    }

    private suspend fun refreshReview(movieID: Int) {
        try {
            val response = apiHelper.getReview(movieID)
            if (response.isSuccessful) {
                val review = response.body()
                if (review != null) {
                    coroutineScope {
                        launch(Dispatchers.IO) {
                            reviewDao.insert(review)
                        }
                    }
                }
            } else{
                Log.e("test", "refreshMovieDetail: Error fetching data")
            }
        } catch (e: Exception){

        }
    }

    suspend fun getTrailer(movieID: Int): Flow<String?> {
        refreshTrailer(movieID)
        coroutineScope {
            launch(Dispatchers.IO)
            {
                trailer = trailerDao.get(movieID)
            }
        }
        return trailer
    }

    private suspend fun refreshTrailer(movieID: Int) {
        try {
            val response = apiHelper.getTrailer(movieID)
            if (response.isSuccessful) {
                val trailer = response.body()
                if (trailer != null) {
                    coroutineScope {
                        launch(Dispatchers.IO) {
                            trailerDao.insert(trailer)
                        }
                    }
                }
            } else{
                Log.e("test", "refreshMovieDetail: Error fetching data")
            }
        } catch (e: Exception){ }
    }
}