package com.riftar.moviedb2021.data.db.dao

import androidx.room.*
import com.riftar.moviedb2021.data.model.Fav
import kotlinx.coroutines.flow.Flow

@Dao
interface FavDao{

    @Query("SELECT * FROM tb_fav")
    fun getAllFav(): Flow<List<Fav>>

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insert(fav: Fav)

    @Delete
    fun delete(fav: Fav)
}