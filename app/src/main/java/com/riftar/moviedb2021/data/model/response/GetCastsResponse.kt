package com.riftar.moviedb2021.data.model.response

import androidx.room.Entity
import androidx.room.PrimaryKey
import androidx.room.TypeConverters
import com.google.gson.annotations.SerializedName
import com.riftar.moviedb2021.data.db.converter.CastListConverter
import com.riftar.moviedb2021.data.db.converter.ReviewListConverter
import com.riftar.moviedb2021.data.model.Cast
@Entity(tableName = "tb_cast")
data class GetCastsResponse(
    @PrimaryKey(autoGenerate = false)
    @SerializedName("id")
    val id : Int? = null,
    @TypeConverters(CastListConverter::class)
    @SerializedName("cast")
    val cast: List<Cast>? = null
)