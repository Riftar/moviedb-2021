package com.riftar.moviedb2021.data.db.converter

import androidx.room.TypeConverter
import com.google.gson.Gson
import com.google.gson.reflect.TypeToken
import com.riftar.moviedb2021.data.model.AuthorDetails
import com.riftar.moviedb2021.data.model.Genre
import com.riftar.moviedb2021.data.model.Trailer


open class TrailerListConverter {
    @TypeConverter
    fun fromString(value: String): List<Trailer>? {
        val listType = object : TypeToken<List<Trailer>>() {}.type
        return Gson().fromJson<List<Trailer>>(value, listType)
    }

    @TypeConverter
    fun fromList(list: List<Trailer>?): String {
        val gson = Gson()
        return gson.toJson(list)
    }
}