package com.riftar.moviedb2021.data.model


import androidx.room.Entity
import com.google.gson.annotations.SerializedName

data class Cast(
    @SerializedName("character")
    var character: String? = null,
    @SerializedName("name")
    var name: String? = null,
    @SerializedName("original_name")
    var originalName: String? = null,
    @SerializedName("profile_path")
    var profilePath: String? = null
)