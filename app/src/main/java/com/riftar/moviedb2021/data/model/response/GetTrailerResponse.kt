package com.riftar.moviedb2021.data.model.response

import androidx.room.Entity
import androidx.room.PrimaryKey
import androidx.room.TypeConverters
import com.google.gson.annotations.SerializedName
import com.riftar.moviedb2021.data.db.converter.TrailerListConverter
import com.riftar.moviedb2021.data.model.Trailer
@Entity(tableName = "tb_trailer")
data class GetTrailerResponse(
    @PrimaryKey(autoGenerate = false)
    @SerializedName("id")
    val id : Int? = null,
    @TypeConverters(TrailerListConverter::class)
    @SerializedName("results")
    val results: List<Trailer>? = null
)