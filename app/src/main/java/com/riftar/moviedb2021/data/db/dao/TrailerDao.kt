package com.riftar.moviedb2021.data.db.dao

import androidx.room.*
import com.riftar.moviedb2021.data.model.Review
import com.riftar.moviedb2021.data.model.response.GetReviewResponse
import com.riftar.moviedb2021.data.model.response.GetTrailerResponse
import kotlinx.coroutines.flow.Flow
@Dao
interface TrailerDao {
    @Query("SELECT results FROM tb_trailer WHERE id LIKE :movieid LIMIT 1")
    fun get(movieid: Int): Flow<String?>

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insert(trailer: GetTrailerResponse)

    @Delete
    fun delete(trailer: GetTrailerResponse)
}