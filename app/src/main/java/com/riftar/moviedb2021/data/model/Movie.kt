package com.riftar.moviedb2021.data.model

import androidx.room.*
import com.google.gson.annotations.SerializedName
import com.riftar.moviedb2021.data.db.converter.AuthorConverter
import com.riftar.moviedb2021.data.db.converter.CastListConverter
import com.riftar.moviedb2021.data.db.converter.GenreListConverter
import com.riftar.moviedb2021.data.db.converter.ReviewListConverter
import java.io.Serializable
@Entity(tableName = "tb_movie")
data class Movie(

    @PrimaryKey(autoGenerate = true)
    var uId: Boolean? = null,

    @SerializedName("video")
    var video: Boolean? = null,

    @SerializedName("poster_path")
    var posterPath: String? = null,

    @SerializedName("id")
    var id: Int? = null,

    @SerializedName("backdrop_path")
    var backdropPath: String? = null,

    @SerializedName("original_title")
    var originalTitle: String? = null,

    @SerializedName("title")
    var title: String? = null,

    @SerializedName("vote_average")
    var voteAverage: Float? = null,

    @SerializedName("overview")
    var overview: String? = null,

    @SerializedName("release_date")
    var releaseDate: String? = null,

    @SerializedName("runtime")
    var runtime: Int? = null,
    @SerializedName("genres")
    var genre: List<Genre>? = null,

    var isLiked: Boolean = false,
    var category: String? = null
) : Serializable
