package com.riftar.moviedb2021.data.network

import com.riftar.moviedb2021.data.model.Movie
import com.riftar.moviedb2021.data.model.response.GetCastsResponse
import com.riftar.moviedb2021.data.model.response.GetReviewResponse
import com.riftar.moviedb2021.data.model.response.GetTrailerResponse
import com.riftar.moviedb2021.data.model.response.GetListMovieResponse
import retrofit2.Response
import retrofit2.http.GET
import retrofit2.http.Path
import retrofit2.http.Query

interface API {
    @GET("movie/popular")
    suspend fun getPopular(@Query("page") page: Int): Response<GetListMovieResponse>

    @GET("movie/now_playing")
    suspend fun getNowPlaying(@Query("page") page: Int): Response<GetListMovieResponse>

    @GET("movie/top_rated")
    suspend fun getTopRated(@Query("page") page: Int): Response<GetListMovieResponse>

    @GET("movie/{movie_id}")
    suspend fun getMovie(
        @Path("movie_id") movieID: Int): Response<Movie>

    @GET("movie/{movie_id}/reviews")
    suspend fun getReview(
        @Path("movie_id") movieId: Int): Response<GetReviewResponse>

    @GET("movie/{movie_id}/videos")
    suspend fun getTrailer(
        @Path("movie_id") movieId: Int): Response<GetTrailerResponse>

    @GET("movie/{movie_id}/credits")
    suspend fun getCast(
        @Path("movie_id") movieId: Int): Response<GetCastsResponse>

//    @GET("discover/movie")
//    fun setMovies(
//        @Query("api_key") apiKey: String = "3f330e22391fa290f80e611e5f819d6a",
//        @Query("page") page: Int,
//        @Query("with_genres") genre: String
//    ): Call<GetMoviesResponse>
//


}