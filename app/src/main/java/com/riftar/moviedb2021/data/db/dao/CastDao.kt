package com.riftar.moviedb2021.data.db.dao

import androidx.room.*
import com.riftar.moviedb2021.data.model.response.GetCastsResponse
import kotlinx.coroutines.flow.Flow

@Dao
interface CastDao {
    @Query("SELECT `cast` FROM tb_cast WHERE id LIKE :movieid LIMIT 1")
    fun get(movieid: Int): Flow<String?>

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insert(cast: GetCastsResponse)

    @Delete
    fun delete(cast: GetCastsResponse)
}