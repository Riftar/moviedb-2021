package com.riftar.moviedb2021.data.network

import com.riftar.moviedb2021.data.model.Movie
import com.riftar.moviedb2021.data.model.response.GetCastsResponse
import com.riftar.moviedb2021.data.model.response.GetReviewResponse
import com.riftar.moviedb2021.data.model.response.GetTrailerResponse
import com.riftar.moviedb2021.data.model.response.GetListMovieResponse
import retrofit2.Response
import javax.inject.Inject

class ApiHelperImp @Inject constructor(private val api: API) : ApiHelper {
    override suspend fun getPopular(page: Int): Response<GetListMovieResponse> = api.getPopular(page)
    override suspend fun getNowPlaying(page: Int): Response<GetListMovieResponse> = api.getNowPlaying(page)
    override suspend fun getTopRated(page: Int): Response<GetListMovieResponse> = api.getTopRated(page)
    override suspend fun getMovie(movieID: Int): Response<Movie> = api.getMovie(movieID)
    override suspend fun getTrailer(movieID: Int): Response<GetTrailerResponse> = api.getTrailer(movieID)
    override suspend fun getReview(movieID: Int): Response<GetReviewResponse> = api.getReview(movieID)
    override suspend fun getCast(movieID: Int): Response<GetCastsResponse> = api.getCast(movieID)

}