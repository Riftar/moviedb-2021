package com.riftar.moviedb2021.data.db.converter

import androidx.room.TypeConverter
import com.google.gson.Gson
import com.google.gson.reflect.TypeToken
import com.riftar.moviedb2021.data.model.AuthorDetails
import com.riftar.moviedb2021.data.model.Cast

open class AuthorConverter {
    @TypeConverter
    fun fromString(value: String): AuthorDetails? {
        val objType = object : TypeToken<AuthorDetails>() {}.type
        return Gson().fromJson<AuthorDetails>(value, objType)
    }

    @TypeConverter
    fun fromObj(obj: AuthorDetails?): String {
        val gson = Gson()
        return gson.toJson(obj)
    }
}