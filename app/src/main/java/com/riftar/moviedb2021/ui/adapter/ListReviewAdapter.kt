package com.riftar.moviedb2021.ui.adapter

import android.text.Html
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.webkit.URLUtil
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.bumptech.glide.load.resource.bitmap.CenterCrop
import com.riftar.moviedb2021.R
import com.riftar.moviedb2021.data.model.Review
import kotlinx.android.synthetic.main.item_cast.view.*
import kotlinx.android.synthetic.main.item_review.view.*

class ListReviewAdapter(private val listItem: List<Review>) : RecyclerView.Adapter<ListReviewAdapter.ListReviewHolder>() {
    inner class ListReviewHolder(itemView: View): RecyclerView.ViewHolder(itemView) {
        fun bind(item: Review) {
            val avatarPath = item.authorDetails?.avatarPath
            val isUrl = avatarPath?.contains("http")
            val finalUrl = if (isUrl == true) {
                avatarPath.removePrefix("/")
            } else "https://image.tmdb.org/t/p/w185$avatarPath"
            if (!avatarPath.isNullOrEmpty()){
                Glide.with(itemView.context)
                    .load(finalUrl)
                    .transform(CenterCrop())
                    .into(itemView.ivIconUserReview)
            }
            val content = Html.fromHtml(item.content)
            itemView.tvNameReview.text = item.author
            itemView.tvDescriptionReview.text = content
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ListReviewHolder {
        val itemView = LayoutInflater.from(parent.context)
            .inflate(R.layout.item_review, parent, false)
        return ListReviewHolder(itemView)
    }

    override fun getItemCount(): Int {
        Log.d("test", "getItemCount: ${listItem.size}")
        return listItem.size
    }

    override fun onBindViewHolder(holder: ListReviewAdapter.ListReviewHolder, position: Int) {
        holder.bind(listItem[position])
    }

}