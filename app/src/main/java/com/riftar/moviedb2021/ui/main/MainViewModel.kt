package com.riftar.moviedb2021.ui.main

import android.util.Log
import androidx.hilt.lifecycle.ViewModelInject
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.riftar.moviedb2021.data.model.Movie
import com.riftar.moviedb2021.data.repository.MovieRepository
import com.riftar.moviedb2021.utils.NetworkHelper
import com.riftar.moviedb2021.utils.Resource
import kotlinx.coroutines.cancel
import kotlinx.coroutines.flow.collect
import kotlinx.coroutines.launch

class MainViewModel @ViewModelInject constructor(
    private val networkHelper: NetworkHelper,
    private val movieRepository: MovieRepository
): ViewModel() {
    val listMovie = MutableLiveData<Resource<List<Movie>>>()
    val movie = MutableLiveData<Resource<Movie>>()
    val isConnected = MutableLiveData<Boolean>()
    val currentCategory : LiveData<String> get() = _currentCategory
    private val _currentCategory = MutableLiveData<String>()


    fun setCategory(category: String){
        _currentCategory.postValue(category)
    }

    fun getPopular(page: Int){
        Log.d("test", "getPopular: called")
        viewModelScope.launch {
            listMovie.postValue(Resource.loading(null))
            movieRepository.getPopular(page).collect {
                listMovie.postValue(Resource.success(it))
            }
        }
    }

    fun getNowPlaying(page: Int) {
        viewModelScope.launch {
            listMovie.postValue(Resource.loading(null))
            movieRepository.getNowPlaying(page).collect {
                listMovie.postValue(Resource.success(it))
            }
        }
    }


    fun getTopRated(page: Int) {
        viewModelScope.launch {
            listMovie.postValue(Resource.loading(null))
            movieRepository.getTopRated(page).collect {
                listMovie.postValue(Resource.success(it))
            }
        }
    }

    fun getMovie(movieID: Int){
        viewModelScope.launch {
            if (networkHelper.isNetworkConnected()) {
                isConnected.postValue(true)
                movie.postValue(Resource.loading(null))
                movieRepository.getMovie(movieID).let {
                    if (it.isSuccessful) {
                        val result = it.body()
                        movie.postValue(Resource.success(result))
                    } else
                        movie.postValue(Resource.error(it.errorBody().toString(), null))
                }
            } else {
                isConnected.postValue(false)
            }
        }
    }

    fun getMovieLocal(movieID: Int) {
        viewModelScope.launch {
            movieRepository.getMovieLocal(movieID).collect {
                movie.postValue(Resource.success(it))
            }
        }
    }

    fun onClickFav(data: Movie) {
        viewModelScope.launch{
            val isLiked = data.isLiked
            data.isLiked = !isLiked
            data.id?.let { movieRepository.updateLike(!isLiked, it) }
            if(!isLiked == true){
                movieRepository.addToFav(data)
            } else {
                movieRepository.deleteFromFav(data)
            }

        }
    }

}