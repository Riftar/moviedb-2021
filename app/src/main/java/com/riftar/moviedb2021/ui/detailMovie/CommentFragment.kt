package com.riftar.moviedb2021.ui.detailMovie

import android.content.Intent
import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.fragment.app.viewModels
import androidx.lifecycle.Observer
import androidx.recyclerview.widget.LinearLayoutManager
import com.riftar.moviedb2021.R
import com.riftar.moviedb2021.data.model.Review
import com.riftar.moviedb2021.ui.adapter.ListReviewAdapter
import com.riftar.moviedb2021.ui.main.MainViewModel
import com.riftar.moviedb2021.ui.review.ReviewActivity
import dagger.hilt.android.AndroidEntryPoint
import kotlinx.android.synthetic.main.fragment_about.*
import kotlinx.android.synthetic.main.fragment_comment.*

@AndroidEntryPoint
class CommentFragment : Fragment() {

    private lateinit var adapter: ListReviewAdapter
    private val viewModel : DetailViewModel by viewModels()
    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_comment, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        if (arguments != null){
            val movieId = arguments?.getInt("movie_id")
            movieId?.let {
                getAllData(it)
                observeData()
                onClickGroup(movieId)
            }
        }
    }

    private fun onClickGroup(movieId: Int) {
        btnMoreReview.setOnClickListener {
            val intent = Intent(activity, ReviewActivity::class.java)
            intent.putExtra("movie_id", movieId)
            startActivity(intent)
        }
    }

    private fun observeData() {
        viewModel.listReview.observe(viewLifecycleOwner, Observer {
//                resource ->
//            resource.data.let {
            reviews ->
                reviews?.let {
                    btnMoreReview.visibility = if (reviews.size > 2) View.VISIBLE else View.GONE
                    setRecyclerview(it)
                }
//            }
        })

    }

    private fun getAllData(movieId: Int) {
        viewModel.getReview(movieId)
    }

    private fun setRecyclerview(data: List<Review>){
        val reviews = data.takeLast(2)
        adapter = ListReviewAdapter(reviews)
        val layoutManager = LinearLayoutManager(activity)
        rvReview.layoutManager = layoutManager
        rvReview.setHasFixedSize(true)
        rvReview.isNestedScrollingEnabled = true
        rvReview.adapter = adapter
    }
}