package com.riftar.moviedb2021.ui.detailMovie

import android.util.Log
import androidx.hilt.lifecycle.ViewModelInject
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.riftar.moviedb2021.data.model.Cast
import com.riftar.moviedb2021.data.model.Review
import com.riftar.moviedb2021.data.model.Trailer
import com.riftar.moviedb2021.data.repository.DetailRepository
import com.riftar.moviedb2021.data.repository.MovieRepository
import com.riftar.moviedb2021.utils.NetworkHelper
import com.riftar.moviedb2021.utils.Utils.toCast
import com.riftar.moviedb2021.utils.Utils.toReview
import com.riftar.moviedb2021.utils.Utils.toTrailer
import kotlinx.coroutines.flow.collect
import kotlinx.coroutines.launch

class DetailViewModel@ViewModelInject constructor(
    private val networkHelper: NetworkHelper,
    private val detailRepository: DetailRepository
): ViewModel() {
    val trailer =  MutableLiveData<Trailer>()
    val listCast = MutableLiveData<List<Cast>>()
    val listReview = MutableLiveData<List<Review>>()

    fun getReview(movieID: Int){
        viewModelScope.launch {
            detailRepository.getReview(movieID).collect { stringReviews->
                stringReviews?.toReview().let {
                    if (it != null){
                        listReview.postValue(it)
                    }
                }
            }
        }
    }

    fun getCast(movieID: Int){
        viewModelScope.launch {
            detailRepository.getCast(movieID).collect { stringReviews->
                stringReviews?.toCast().let {
                    Log.d("test", "getCast: $it")
                    if (it != null){
                        listCast.postValue(it)
                    }
                }
            }
        }
    }

    fun getTrailer(movieID: Int){
        viewModelScope.launch {
            detailRepository.getTrailer(movieID).collect { stringReviews->
                stringReviews?.toTrailer().let {
                    if (it != null){
                        trailer.postValue(it.get(0))
                    }
                }
            }
        }
    }
}