package com.riftar.moviedb2021.ui.favorite

import androidx.hilt.lifecycle.ViewModelInject
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.riftar.moviedb2021.data.model.Movie
import com.riftar.moviedb2021.data.repository.MovieRepository
import com.riftar.moviedb2021.utils.NetworkHelper
import kotlinx.coroutines.flow.collect
import kotlinx.coroutines.launch

class FavoriteViewModel @ViewModelInject constructor(
    private val networkHelper: NetworkHelper,
    private val mainRepository: MovieRepository
): ViewModel() {
    val listFavorite = MutableLiveData<List<Movie>>()

    fun getListFav(){
        viewModelScope.launch {
            mainRepository.getFavorite().collect { list->
                val listMovie = mutableListOf<Movie>()
                list.forEach {
                    it.movie?.let { movie -> listMovie.add(movie) }
                }
                listFavorite.postValue(listMovie)
            }
        }
    }

    fun onClickFav(data: Movie) {
        viewModelScope.launch{
            val isLiked = data.isLiked
            data.isLiked = !isLiked
            data.id?.let { mainRepository.updateLike(!isLiked, it) }
            if(!isLiked == true){
                mainRepository.addToFav(data)
            } else {
                mainRepository.deleteFromFav(data)
            }

        }
    }

}