package com.riftar.moviedb2021.ui.detailMovie

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.Window
import androidx.fragment.app.DialogFragment
import com.pierfrancescosoffritti.androidyoutubeplayer.core.player.YouTubePlayer
import com.pierfrancescosoffritti.androidyoutubeplayer.core.player.listeners.AbstractYouTubePlayerListener
import com.riftar.moviedb2021.R
import kotlinx.android.synthetic.main.fragment_youtube_dialog.*

class YoutubeDialog : DialogFragment() {
    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        dialog!!.requestWindowFeature(Window.FEATURE_NO_TITLE)
        val p = dialog!!.window!!.attributes
        p.width = ViewGroup.LayoutParams.MATCH_PARENT
        p.dimAmount = 0f
        return inflater.inflate(R.layout.fragment_youtube_dialog, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        if (arguments != null){
            val youtubeKey = arguments?.getString("youtubeKey")
            if (youtubeKey != null) {
                setupPlayer(youtubeKey)
            }
        }

    }

    private fun setupPlayer(youtubeKey: String) {
        lifecycle.addObserver(youtube_player_view)
        youtube_player_view.addYouTubePlayerListener(object :
            AbstractYouTubePlayerListener() {
            override fun onReady(youTubePlayer: YouTubePlayer) {
                youTubePlayer.cueVideo(youtubeKey, 0f)
            }
        })
    }

    override fun onPause() {
        super.onPause()
        youtube_player_view.release()
    }
    override fun onDestroy() {
        super.onDestroy()
    }
}