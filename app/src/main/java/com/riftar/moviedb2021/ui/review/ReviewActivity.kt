package com.riftar.moviedb2021.ui.review

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import androidx.activity.viewModels
import androidx.lifecycle.Observer
import androidx.recyclerview.widget.LinearLayoutManager
import com.riftar.moviedb2021.R
import com.riftar.moviedb2021.data.model.Review
import com.riftar.moviedb2021.ui.adapter.ListReviewAdapter
import com.riftar.moviedb2021.ui.detailMovie.DetailViewModel
import com.riftar.moviedb2021.ui.main.MainViewModel
import dagger.hilt.android.AndroidEntryPoint
import kotlinx.android.synthetic.main.activity_review.*
import kotlinx.android.synthetic.main.fragment_comment.*

@AndroidEntryPoint
class ReviewActivity : AppCompatActivity() {
    private lateinit var adapter : ListReviewAdapter
    private val viewModel: DetailViewModel by viewModels()
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_review)
        if (intent.extras != null) {
            val bundle = intent.extras
            val movieID = bundle?.getInt("movie_id")
            if (movieID != null) {
                getAllData(movieID)
                observeData(movieID)
                onClickGroup()
            }
        }
    }

    private fun onClickGroup() {
        icBack.setOnClickListener {
            onBackPressed()
        }
    }

    private fun getAllData(movieID: Int) {
        viewModel.getReview(movieID)
    }
    private fun observeData(movieID: Int) {
        viewModel.listReview.observe(this, Observer {
//                resource ->
//            resource.data?.let{
                    reviews ->
                setRecyclerview(reviews)
//            }
        })
    }

    private fun setRecyclerview(data: List<Review>){
        adapter = ListReviewAdapter(data)
        val layoutManager = LinearLayoutManager(this)
        rvReviews.layoutManager = layoutManager
        rvReviews.setHasFixedSize(true)
        rvReviews.isNestedScrollingEnabled = false
        rvReviews.adapter = adapter
    }
}