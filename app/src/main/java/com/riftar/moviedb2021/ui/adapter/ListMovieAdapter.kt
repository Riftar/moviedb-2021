package com.riftar.moviedb2021.ui.adapter

import android.graphics.drawable.Drawable
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.bumptech.glide.load.DataSource
import com.bumptech.glide.load.engine.GlideException
import com.bumptech.glide.load.resource.bitmap.CenterCrop
import com.bumptech.glide.request.RequestListener
import com.bumptech.glide.request.target.Target
import com.riftar.moviedb2021.R
import com.riftar.moviedb2021.data.model.Movie
import kotlinx.android.synthetic.main.item_movie.view.*

interface OnMovieClickCallback {
    fun onItemClicked(data: Movie)
    fun onItemLiked(data: Movie)
}
class ListMovieAdapter(private val listener: OnMovieClickCallback) : RecyclerView.Adapter<ListMovieAdapter.ListMovieHolder>() {
    private val listItem = ArrayList<Movie>()
    inner class ListMovieHolder(itemView: View): RecyclerView.ViewHolder(itemView) {
        fun bind(item: Movie) {
            Glide.with(itemView.context)
                .load("https://image.tmdb.org/t/p/w342${item.posterPath}")
                .transform(CenterCrop())
                .addListener(object : RequestListener<Drawable> {
                    override fun onLoadFailed(e: GlideException?, model: Any?, target: Target<Drawable>?, isFirstResource: Boolean
                    ): Boolean {
                        return false
                    }

                    override fun onResourceReady(resource: Drawable?, model: Any?, target: Target<Drawable>?, dataSource: DataSource?, isFirstResource: Boolean
                    ): Boolean {
                        itemView.progressPoster.visibility = View.GONE
                        return false
                    }

                })
                .into(itemView.ivMoviePoster)
            itemView.tvMovieTitle.text = item.originalTitle
            itemView.tvReleaseDate.text = item.releaseDate
            itemView.tvMovieRating.text = item.voteAverage.toString()
            var genre = ""
            item.genre?.forEach {
                genre += "${it.name}/"
            }
            itemView.tvGenre.text = genre
            itemView.setOnClickListener { listener.onItemClicked(item) }
            val liked = itemView.resources.getDrawable(R.drawable.ic_like_full)
            val disliked = itemView.resources.getDrawable(R.drawable.ic_like_empty)
            itemView.ivLike.apply {
                setImageDrawable(if (item.isLiked) liked else disliked)
                setOnClickListener { listener.onItemLiked(item) }
            }
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ListMovieHolder {
        val itemView = LayoutInflater.from(parent.context)
            .inflate(R.layout.item_movie, parent, false)
        return ListMovieHolder(itemView)
    }

    override fun getItemCount(): Int {
        return listItem.size
    }

    override fun onBindViewHolder(holder: ListMovieHolder, position: Int) {
        holder.bind(listItem[position])
    }

    fun insertData(items: List<Movie>){
        listItem.addAll(items)
    }
    fun resetData(){
        listItem.removeAll(listItem)
    }

}