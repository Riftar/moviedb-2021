package com.riftar.moviedb2021.ui.detailMovie

import android.graphics.drawable.Drawable
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import androidx.activity.viewModels
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentManager
import androidx.fragment.app.FragmentPagerAdapter
import androidx.fragment.app.FragmentTransaction
import androidx.lifecycle.Observer
import com.bumptech.glide.Glide
import com.bumptech.glide.load.DataSource
import com.bumptech.glide.load.engine.GlideException
import com.bumptech.glide.load.resource.bitmap.CenterCrop
import com.bumptech.glide.request.RequestListener
import com.bumptech.glide.request.target.Target
import com.riftar.moviedb2021.R
import com.riftar.moviedb2021.data.model.Movie
import com.riftar.moviedb2021.data.model.response.GetReviewResponse
import com.riftar.moviedb2021.ui.main.MainViewModel
import com.riftar.moviedb2021.utils.Status
import dagger.hilt.android.AndroidEntryPoint
import kotlinx.android.synthetic.main.activity_detail_movie.*

@AndroidEntryPoint
class DetailMovieActivity : AppCompatActivity() {

    private val viewModel : DetailViewModel by viewModels()
    private val mainViewModel : MainViewModel by viewModels()
    private lateinit var pages: Array<Fragment>
    private val pageTitle = arrayOf("About", "Comment")
    private var youtubeKey = ""
    private lateinit var currentMovie : Movie

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_detail_movie)
        if (intent.extras != null) {
            val bundle = intent.extras
            val movieID = bundle?.getInt("movie_id")
            if (movieID != null) {
                getAllData(movieID)
                observeData(movieID)
                onClickGroup()
            }
        }
    }

    private fun onClickGroup() {
        btnPlayTrailer.setOnClickListener {
            showTrailer()
        }

        icBack.setOnClickListener {
            onBackPressed()
        }
    }

    private fun showTrailer() {
        val youTubeFragment = YoutubeDialog()
        val bundle = Bundle()
        youtubeKey.let { if (it.isEmpty().not()) bundle.putString("youtubeKey", it) }
        youTubeFragment.arguments = bundle
        val ft: FragmentTransaction = this.supportFragmentManager.beginTransaction()
        ft.addToBackStack(null)
        youTubeFragment.show(ft, "dialog")
    }

    private fun observeData(movieID: Int) {
        mainViewModel.movie.observe(this, Observer { resource ->
            if (resource.status  == Status.ERROR){
                mainViewModel.getMovieLocal(movieID)
            }else{
                 resource.data.let { movie ->
                    movie?.let {
                        currentMovie = it
                        setupView(it)
                        setupFragments(movieID)
                    }
                }
            }
        })

        viewModel.trailer.observe(this, Observer { trailer ->
                if (trailer != null){
                    youtubeKey = trailer.key.toString()
                    btnPlayTrailer.visibility = if (progressBackdrop.visibility == View.GONE) View.VISIBLE else View.GONE
                } else{
                    btnPlayTrailer.visibility = View.GONE
                }
        })
    }

    private fun getAllData(movieID: Int) {
        mainViewModel.getMovie(movieID)
        viewModel.getTrailer(movieID)
    }

    private fun setupView(data: Movie) {
        tvDetailMovieTitle.text = data.originalTitle
        tvDetailMovieYear.text = data.releaseDate
        var genre = ""
        data.genre?.forEach {
            genre += "${it.name}/"
        }
        tvGenre.text = genre
        tvMovieRating.text = data.voteAverage.toString()

        loadImage(data)
    }


    private fun loadImage(data: Movie) {
        Glide.with(this)
            .load("https://image.tmdb.org/t/p/w500${data.posterPath}")
            .transform(CenterCrop())
            .addListener(object : RequestListener<Drawable> {
                override fun onLoadFailed(
                    e: GlideException?,
                    model: Any?,
                    target: Target<Drawable>?,
                    isFirstResource: Boolean
                ): Boolean {
                    return false
                }

                override fun onResourceReady(
                    resource: Drawable?,
                    model: Any?,
                    target: Target<Drawable>?,
                    dataSource: DataSource?,
                    isFirstResource: Boolean
                ): Boolean {
                    progressPoster.visibility = View.GONE
                    return false
                }

            })
            .into(ivPoster)

        Glide.with(this)
            .load("https://image.tmdb.org/t/p/w1280${data.backdropPath}")
            .transform(CenterCrop())
            .addListener(object : RequestListener<Drawable> {
                override fun onLoadFailed(
                    e: GlideException?,
                    model: Any?,
                    target: Target<Drawable>?,
                    isFirstResource: Boolean
                ): Boolean {
                    return false
                }

                override fun onResourceReady(
                    resource: Drawable?,
                    model: Any?,
                    target: Target<Drawable>?,
                    dataSource: DataSource?,
                    isFirstResource: Boolean
                ): Boolean {
                    progressBackdrop.visibility = View.GONE
                    return false
                }

            })
            .into(ivBackdrop)
    }


    private fun setupFragments(movieID: Int) {
        pages = arrayOf(
            AboutFragment().apply {
                val bundle = Bundle()
                if (::currentMovie.isInitialized) bundle.putSerializable("selected_movie", currentMovie)
                this.arguments = bundle
            },
            CommentFragment().apply {
                val bundle = Bundle()
                bundle.putInt("movie_id", movieID)
                this.arguments = bundle
            }
        )
        vpDetailMovie.adapter = MyPagerAdapter(supportFragmentManager)
        vpDetailMovie.offscreenPageLimit = pages.size
        tabDetail.setupWithViewPager(vpDetailMovie)
        tabDetail.setSelectedTabIndicatorColor(resources.getColor(R.color.colorPrimary))
    }

    inner class MyPagerAdapter(fm: FragmentManager) : FragmentPagerAdapter(fm) {
        override fun getItem(position: Int): Fragment {
            return pages[position]
        }

        override fun getCount(): Int {
            return pages.size
        }

        override fun getPageTitle(position: Int): CharSequence? {
            return pageTitle[position]
        }
    }
}