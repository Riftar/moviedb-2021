package com.riftar.moviedb2021.ui.favorite

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import androidx.activity.viewModels
import androidx.lifecycle.Observer
import androidx.recyclerview.widget.LinearLayoutManager
import com.riftar.moviedb2021.R
import com.riftar.moviedb2021.data.model.Movie
import com.riftar.moviedb2021.ui.adapter.ListMovieAdapter
import com.riftar.moviedb2021.ui.adapter.OnMovieClickCallback
import dagger.hilt.android.AndroidEntryPoint
import kotlinx.android.synthetic.main.activity_detail_movie.*
import kotlinx.android.synthetic.main.activity_detail_movie.icBack
import kotlinx.android.synthetic.main.activity_favorite.*
import kotlinx.android.synthetic.main.activity_main.*

@AndroidEntryPoint
class FavoriteActivity : AppCompatActivity(), OnMovieClickCallback {
    private lateinit var adapter : ListMovieAdapter
    private val viewModel : FavoriteViewModel by viewModels()
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_favorite)
        getAllData()
        observeData()
        onClickGroup()
    }

    private fun getAllData() {
        viewModel.getListFav()
    }

    private fun onClickGroup() {
        icBack.setOnClickListener {
            onBackPressed()
        }
    }

    private fun observeData() {
        viewModel.listFavorite.observe(this, Observer {
            setRecyclerview(it)
        })
    }
    private fun setRecyclerview(data: List<Movie>){
        adapter = ListMovieAdapter(this)
        adapter.insertData(data)
        val layoutManager = LinearLayoutManager(this)
        rvFavorite.layoutManager = layoutManager
        rvFavorite.setHasFixedSize(true)
        rvFavorite.isNestedScrollingEnabled = false
        rvFavorite.adapter = adapter
    }

    override fun onItemClicked(data: Movie) {
    }

    override fun onItemLiked(data: Movie) {
        viewModel.onClickFav(data)
    }
}