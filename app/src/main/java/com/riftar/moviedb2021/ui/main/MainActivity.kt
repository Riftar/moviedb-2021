package com.riftar.moviedb2021.ui.main

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import android.view.View
import android.widget.Toast
import androidx.activity.viewModels
import androidx.lifecycle.Observer
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.google.android.material.bottomsheet.BottomSheetDialog
import com.riftar.moviedb2021.R
import com.riftar.moviedb2021.data.model.Movie
import com.riftar.moviedb2021.ui.detailMovie.DetailMovieActivity
import com.riftar.moviedb2021.ui.adapter.ListMovieAdapter
import com.riftar.moviedb2021.ui.adapter.OnMovieClickCallback
import com.riftar.moviedb2021.ui.favorite.FavoriteActivity
import com.riftar.moviedb2021.utils.Status
import dagger.hilt.android.AndroidEntryPoint
import kotlinx.android.synthetic.main.activity_main.*
import kotlinx.android.synthetic.main.category_bottomsheet.view.*

@AndroidEntryPoint
class MainActivity : AppCompatActivity(), OnMovieClickCallback {

    companion object {
        const val TOP_RATED = "Top Rated"
        const val NOW_PLAYING = "Now Playing"
        const val POPULAR = "Popular"
    }

    lateinit var adapter : ListMovieAdapter
    private val viewModel : MainViewModel by viewModels()
    private var pages = 1
    private var isLoading = false
    val TAG = "test"
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        getAllData()
        observeData()
        onClickGroup()
        Log.d(TAG, "onCreate: called")
    }

    override fun onResume() {
        super.onResume()
    }

    private fun onClickGroup() {

        extFab.setOnClickListener {
            showCategoryBottomSheet()
        }

        ivLike.setOnClickListener {
            val intent = Intent(this, FavoriteActivity::class.java)
            startActivity(intent)
        }
    }

    private fun observeData() {
        viewModel.listMovie.observe(this, Observer { resource ->
            when(resource.status){
                Status.LOADING ->{
                    isLoading = true
                    progressBar.visibility = View.VISIBLE
                }
                Status.SUCCESS ->{
                    resource.data.let { listMovie ->
                        listMovie?.let {
                            Log.d(TAG, "observeData: $it")
                            setRecyclerview(it)
                        }
                        progressBar.visibility = View.GONE
                        isLoading = false
                    }
                }
                Status.ERROR -> {
                    progressBar.visibility = View.GONE
                    isLoading = false
                }
            }

        })

        viewModel.currentCategory.observe(this, Observer {category ->
            when(category){
                POPULAR -> {
                    extFab.text = POPULAR
                    extFab.icon = resources.getDrawable(R.drawable.ic_like_full)
                }
                NOW_PLAYING -> {
                    extFab.text = NOW_PLAYING
                    extFab.icon = resources.getDrawable(R.drawable.ic_nowplay)
                }
                TOP_RATED -> {
                    extFab.text = TOP_RATED
                    extFab.icon = resources.getDrawable(R.drawable.ic_toprated)
                }
            }
        })
    }

    private fun getAllData() {
        viewModel.getPopular(1)
        viewModel.setCategory(POPULAR)
    }

    private fun setRecyclerview(data: List<Movie>){
        adapter = ListMovieAdapter(this)
        adapter.insertData(data)
        adapter.notifyDataSetChanged()
        val layoutManager = LinearLayoutManager(this)
        rvMovie.isSaveEnabled = true
        rvMovie.layoutManager = layoutManager
        rvMovie.setHasFixedSize(true)
        rvMovie.isNestedScrollingEnabled = false
        rvMovie.adapter = adapter
        val scrollListener = object : RecyclerView.OnScrollListener() {
            override fun onScrolled(recyclerView: RecyclerView, dx: Int, dy: Int) {
                super.onScrolled(recyclerView, dx, dy)
                val length = adapter.itemCount
                if(layoutManager.findLastCompletelyVisibleItemPosition() == length -4){
                    //bottom of list!
                    if (!isLoading) loadMoreData()
                }
            }
        }
        rvMovie.addOnScrollListener(scrollListener)
    }

    private fun loadMoreData() {
        pages++
        viewModel.getPopular(pages)
    }

    override fun onItemClicked(data: Movie) {
        Toast.makeText(this, data.title, Toast.LENGTH_SHORT).show()
        val intent = Intent(this, DetailMovieActivity::class.java)
        intent.putExtra("movie_id", data.id)
        startActivity(intent)
    }

    override fun onItemLiked(data: Movie) {
        viewModel.onClickFav(data)
    }


    private fun showCategoryBottomSheet() {
        val dialog = BottomSheetDialog(this)
        val view = layoutInflater.inflate(R.layout.category_bottomsheet, null)
        view.tvPopular.setOnClickListener {
            viewModel.getPopular(1)
            resetRecyclerView()
            onCategoryClick(POPULAR, dialog)
        }
        view.tvNowPlaying.setOnClickListener {
            resetRecyclerView()
            viewModel.getNowPlaying(1)
            onCategoryClick(NOW_PLAYING, dialog)
        }
        view.tvTopRated.setOnClickListener {
            resetRecyclerView()
            viewModel.getTopRated(1)
            onCategoryClick(TOP_RATED, dialog)
        }

        dialog.setCancelable(true)
        dialog.setContentView(view)
        dialog.show()
    }

    private fun onCategoryClick(category: String, dialog: BottomSheetDialog) {
        viewModel.setCategory(category)
        dialog.dismissWithAnimation = true
        dialog.dismiss()
    }

    private fun resetRecyclerView(){
        pages = 1
        rvMovie.removeAllViews()
        adapter.resetData()
        adapter.notifyDataSetChanged()
    }
}