package com.riftar.moviedb2021.ui.detailMovie

import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import androidx.lifecycle.Observer
import androidx.recyclerview.widget.LinearLayoutManager
import com.riftar.moviedb2021.R
import com.riftar.moviedb2021.data.model.Cast
import com.riftar.moviedb2021.data.model.Movie
import com.riftar.moviedb2021.ui.adapter.ListCastAdapter
import com.riftar.moviedb2021.ui.main.MainViewModel
import dagger.hilt.android.AndroidEntryPoint
import kotlinx.android.synthetic.main.fragment_about.*

@AndroidEntryPoint
class AboutFragment : Fragment() {

    lateinit var castAdapter: ListCastAdapter
    private val viewModel : DetailViewModel by viewModels()

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_about, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        if (arguments != null){
            val movie = arguments?.getSerializable("selected_movie") as Movie?
            Log.d("test", "onViewCreated: $movie")
            if (movie != null) {
                setupView(movie)
                getAllData(movie)
                observeData()
            }
        }
    }

    private fun observeData() {
        viewModel.listCast.observe(viewLifecycleOwner, Observer {
//                resource ->
//            resource.data.let {
                    cast ->
                if (cast != null) {
                    setRecyclerview(cast)
                }
//            }
        })
    }

    private fun getAllData(movie: Movie) {
        movie.id?.let { viewModel.getCast(it) }
    }

    private fun setupView(movie: Movie) {
        tvDetailDescription.text = movie.overview
    }

    private fun setRecyclerview(data: List<Cast>){
        castAdapter = ListCastAdapter(data)
        val layoutManager = LinearLayoutManager(activity, LinearLayoutManager.HORIZONTAL, false)
        rvCast.layoutManager = layoutManager
        rvCast.setHasFixedSize(true)
        rvCast.isNestedScrollingEnabled = false
        rvCast.adapter = castAdapter
    }


}