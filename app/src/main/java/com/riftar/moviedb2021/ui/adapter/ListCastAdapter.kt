package com.riftar.moviedb2021.ui.adapter

import android.graphics.drawable.Drawable
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.bumptech.glide.load.DataSource
import com.bumptech.glide.load.engine.GlideException
import com.bumptech.glide.load.resource.bitmap.CenterCrop
import com.bumptech.glide.request.RequestListener
import com.bumptech.glide.request.target.Target
import com.riftar.moviedb2021.R
import com.riftar.moviedb2021.data.model.Cast
import kotlinx.android.synthetic.main.item_cast.view.*

class ListCastAdapter(private val listItem: List<Cast>) : RecyclerView.Adapter<ListCastAdapter.ListCastHolder>() {
    inner class ListCastHolder(itemView: View): RecyclerView.ViewHolder(itemView) {
        fun bind(item: Cast) {
            val avatarPath = item.profilePath
            val isUrl = avatarPath?.contains("http")
            val finalUrl = if (isUrl == true) {
                avatarPath.removePrefix("/")
            } else "https://image.tmdb.org/t/p/w185$avatarPath"
            if (!avatarPath.isNullOrEmpty()){
                Glide.with(itemView.context)
                    .load("https://image.tmdb.org/t/p/w300${item.profilePath}")
                    .transform(CenterCrop())
                    .addListener(object : RequestListener<Drawable> {
                        override fun onLoadFailed(e: GlideException?, model: Any?, target: Target<Drawable>?, isFirstResource: Boolean
                        ): Boolean {
                            return false
                        }

                        override fun onResourceReady(resource: Drawable?, model: Any?, target: Target<Drawable>?, dataSource: DataSource?, isFirstResource: Boolean
                        ): Boolean {
                            itemView.progressCast.visibility = View.GONE
                            itemView.castPhoto.visibility = View.VISIBLE
                            return false
                        }

                    })
                    .into(itemView.castPhoto)
            } else{
                itemView.progressCast.visibility = View.GONE
                itemView.castPhoto.visibility = View.VISIBLE
            }
            itemView.castName.text = item.name
            itemView.characterName.text = item.character
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ListCastHolder {
        val itemView = LayoutInflater.from(parent.context)
            .inflate(R.layout.item_cast, parent, false)
        return ListCastHolder(itemView)
    }

    override fun getItemCount(): Int {
        return listItem.size
    }

    override fun onBindViewHolder(holder: ListCastAdapter.ListCastHolder, position: Int) {
        holder.bind(listItem[position])
    }

}